'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
    this.belongsTo(models.user, {
      foreignKey: 'user_game_id'
    })
    }
  }
  user_game_history.init({
    games_played: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    games_won: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    games_lose: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'user_game_history',
    tablelName: 'user_game_histories'
  });
  return user_game_history;
};