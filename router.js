const express = require('express');
const router = express.Router();
const userController = require('./controllers/userController')

const { body } = require('express-validator');
// const checkToken = require('./middleware/checkToken');




router.post('user/register',body('email').isEmail(), body('username').notEmpty(),body('password').notEmpty(),userController.register);
router.post('user/login',body('email').isEmail().notEmpty(), body('password').notEmpty() ,userController.login);
// router.get('/info',examplecontroller.info);
// router.get('/bio',examplecontroller.bio);
// router.get('/product/:id',examplecontroller.getProductById);
// router.get('/product',examplecontroller.product);
// router.post('/login',examplecontroller.login);




module.exports = router;