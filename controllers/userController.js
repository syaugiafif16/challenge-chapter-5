
const { user } = require('../models')
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');


module.exports = {
    register: async (req,res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const password = bcrypt.hashSync(req.body.password, 10)
            const data = await user.create({
                username: req.body.username,
                email: req.body.email,
                password: password
            });

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    login: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const user = await user.findOne({ where : {
                email: req.body.email
            }})

            if(!user) {
                return res.json({
                    message : "User not exists!"
                })
            }

            if (bcrypt.compareSync(req.body.password, user.password)) {
                const token = jwt.sign(
                  { id: user.id },
                  'secret',
                  { expiresIn: '6h' }
                )
          
                return res.status(200).json({
                    token: token
                })
              }
            
            return res.json({
                message : "Wrong Password!"
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

  
    // game: (req, res) => {
    //     return res.sendFile(path.join(__dirname, '../game.html'));
    // },
    // home: (req, res) => {
    //     return res.sendFile(path.join(__dirname, '../index.html'));
    // }





}